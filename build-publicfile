#!/bin/sh -e

# this file maintained using git at
# ssh+git://git.debian.org/git/collab-maint/publicfile-installer.git

#
# Copyright (C) 2006, 2007 Joost van Baal
# Copyright (C) 2015 Joost van Baal-Ilić
#
# This file is free software; you can redistribute it and/or modify it under
# the terms of the GNU GPL as published by the FSF; version 3 of the License,
# or any later version. You should have received a copy of the GNU GPL along
# with this file (see COPYING); if not, check
# http://www.gnu.org/copyleft/gpl.html.
#

for f in /etc/publicfile-installer.conf /usr/local/etc/publicfile-installer.conf $HOME/.publicfile-installer.conf
do
    test -f $f && . $f
done

# check wether sources are present; if not, call get-publicfile
check_sources

if test -z "$DIFFDIR"
then
    echo "No valid .debian.tar.gz found"
fi

if test -z "$TARDIR"
then
    echo "No valid upstream .tar.gz found"
fi

if test -z "$TARDIR" -o -z "$DIFFDIR"
then
    if test "$1" = "go"
    then
        echo "Please run get-publicfile to fetch sources."
        exit 1
    elif ask_user "Do you want to fetch sources now? [Yn] " 0
    then
        exec get-publicfile
    else
        echo "OK, you can run get-publicfile later."
    fi
fi

if test -f "$DIFFDIR/publicfile_$DEBVERSION*.deb"
then
    echo "File $DIFFDIR/$DEBFILE already built.  No need to rebuild."
else

    # create build dir, copy sources to build dir
    BUILDDIR=`mktemp -d` || exit 1
    cd $BUILDDIR

    if test -f "$TARDIR/$TARFILE" -a -f "$DIFFDIR/$DIFFFILE" -a -f "$DIFFDIR/$DSCFILE"
    then
        cp $TARDIR/$TARFILE .
        cp $DIFFDIR/$DIFFFILE .
        cp $DIFFDIR/$DSCFILE .
    else
        echo "File '$DIFFFILE' and/or '$DSCFILE' not found in directory '$DIFFDIR'; and/or file '$TARFILE' not found in directory '$TARDIR'."
        echo "Please run get-publicfile to fetch sources."
        exit 1
    fi

    if ! test -L $ORIGFILE
    then
        rm -f $ORIGFILE
        ln -s $TARFILE $ORIGFILE
    fi

    echo "Preparing build of '$DEBFILE' in '$BUILDDIR'..."

    # unpack, patch and build
    dpkg-source -x publicfile*.dsc

    ## tar zxf $TARFILE
    ## cd publicfile-?.??/
    ## zcat ../$DIFFFILE | patch -p1 --quiet
    ## chmod a+x debian/rules

    cd publicfile-?.??/

    echo "Building $DEBFILE in `pwd` (if you won't get a message $DEBFILE is"
    echo "ready, inspect the build log in $BUILDDIR/build.log to see"
    echo "what went wrong)..."
    dpkg-buildpackage -rfakeroot -b -uc >$BUILDDIR/build.log 2>&1

    # clean up build dir
    if ! cp ../publicfile_$DEBVERSION*.deb $DIFFDIR
    then
        echo "Error copying ../publicfile_$DEBVERSION*.deb to $DIFFDIR."
        echo "Inspect `pwd` and $DIFFDIR to see what went wrong."
        exit 1
    fi

    echo " ---------------"
    echo "| $DIFFDIR/$DEBFILE ready, build log in $BUILDDIR/build.log."
    echo " ---------------"

    if test "$1" = "go"
    then
        rm -rf $BUILDDIR
    elif ask_user "Do you want to keep build stuff in $BUILDDIR, e.g. for debugging? [Ny] " 1
    then
        echo "OK, keeping $BUILDDIR.  You can remove it manually later."
    else
        rm -rf $BUILDDIR
    fi
fi

install_publicfile="sudo dpkg -i $DIFFDIR/publicfile_$DEBVERSION*.deb"
if test "$1" = "go"
then
    # non-interactive mode, called from within dpkg
    echo "Remember to run $install_publicfile later."
elif ask_user "Do you want to install the publicfile package now? [Yn] " 0
then
    exec $install_publicfile
else
    echo "OK, you can run $install_publicfile later."
fi
