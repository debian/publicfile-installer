# this file maintained using git at
# ssh+git://git.debian.org/git/collab-maint/publicfile-installer.git

#
# Copyright (C) 2006, 2007, 2008 Joost van Baal
# Copyright (C) 2015 Joost van Baal-Ilić
#
# This file is free software; you can redistribute it and/or modify it under
# the terms of the GNU GPL as published by the FSF; version 3 of the License,
# or any later version. You should have received a copy of the GNU GPL along
# with this file (see COPYING); if not, check
# http://www.gnu.org/copyleft/gpl.html.
#

VERSION = 0.13

srcdir = .

INSTALL = /usr/bin/install -c
man1dir = $(mandir)/man1
INSTALL_DATA = ${INSTALL} -m 644

DISTFILES = README $(srcdir)/Makefile COPYING ChangeLog TODO $(SCRIPTS) $(etc_DATA) $(man_MANS)

bindir = ${exec_prefix}/bin
mandir = ${prefix}/man
exec_prefix = ${prefix}
mkdir_p = mkdir -p --
prefix = /usr/local

distdir = $(PACKAGE)-$(VERSION)

am__tar = tar chof - "$$tardir"

PACKAGE = publicfile-installer

SCRIPTS= \
    get-publicfile \
    build-publicfile

etc_DATA = \
    publicfile-installer.conf

man_MANS = \
    publicfile-installer.1 \
    get-publicfile.1 \
    build-publicfile.1

all:

install: $(SCRIPTS) $(man_MANS)
	test -z "$(DESTDIR)$(bindir)" || $(mkdir_p) "$(DESTDIR)$(bindir)"
	test -z "$(DESTDIR)$(man1dir)" || $(mkdir_p) "$(DESTDIR)$(man1dir)"
	list='$(SCRIPTS)'; for p in $$list; do \
          if test -f "$$p"; then d=; else d="$(srcdir)/"; fi; \
          if test -f $$d$$p; then \
            $(INSTALL) "$$d$$p" "$(DESTDIR)$(bindir)/$$p"; \
          else :; fi; \
        done
	list='$(man_MANS)'; for p in $$list; do \
          if test -f "$$p"; then d=; else d="$(srcdir)/"; fi; \
          if test -f $$d$$p; then \
            $(INSTALL_DATA) "$$d$$p" "$(DESTDIR)$(man1dir)/$$p"; \
          else :; fi; \
        done


uninstall:
	list='$(SCRIPTS)'; for p in $$list; do \
          rm -f "$(DESTDIR)$(bindir)/$$p"; \
        done

clean:

COPYING:
	cp --preserve=all --dereference /usr/share/common-licenses/GPL $@

distdir: $(DISTFILES)
	rm -rf $(distdir)
	mkdir $(distdir)
	list='$(DISTFILES)'; for file in $$list; do \
          if test -f $$file || test -d $$file; then d=.; else d=$(srcdir); fi; \
          cp -pR $$d/$$file $(distdir)$$dir; \
        done

dist: distdir
	tardir=$(distdir) && $(am__tar) | gzip -c >$(distdir).tar.gz
	ls -l $(distdir).tar.gz

publish:
	scp $(distdir).tar.gz beskar.mdcc.cx:www/mdcc.cx/pub/publicfile

.PHONY: all dist distdir install uninstall clean
