Source: publicfile-installer
Section: contrib/net
Priority: optional
Maintainer: Joost van Baal-Ilić <joostvb@debian.org>
Build-Depends: debhelper-compat (= 13), po-debconf
Rules-Requires-Root: no
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/debian/publicfile-installer.git
Vcs-Browser: https://salsa.debian.org/debian/publicfile-installer

Package: publicfile-installer
Architecture: all
Depends: wget, debhelper, fakeroot, sudo, ${misc:Depends}
Description: installer package for the publicfile HTTP and FTP server
 Publicfile is an HTTP and FTP server, written by Daniel J. Bernstein in
 1999; it didn't change a lot after that. Modern features are not
 supported. However, if you're looking for a small, simple and secure
 webserver, which integrates with the runit and daemontools UNIX service
 managers, publicfile will suit your needs.
 .
 This installer package downloads the publicfile .tar.gz file from
 the upstream website, combines it with Debian packaging information
 from the package maintainer's website; then builds a publicfile Debian
 package, and installs that. When installing this installer package,
 one is given the option to postpone downloading and installing
 publicfile.
