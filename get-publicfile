#!/bin/sh

# this file maintained using git at
# ssh+git://git.debian.org/git/collab-maint/publicfile-installer.git

#
# Copyright (C) 2006, 2007 Joost van Baal
# Copyright (C) 2015 Joost van Baal-Ilić
#
# This file is free software; you can redistribute it and/or modify it under
# the terms of the GNU GPL as published by the FSF; version 3 of the License,
# or any later version. You should have received a copy of the GNU GPL along
# with this file (see COPYING); if not, check
# http://www.gnu.org/copyleft/gpl.html.
#

for f in /etc/publicfile-installer.conf /usr/local/etc/publicfile-installer.conf $HOME/.publicfile-installer.conf
do
    test -f $f && . $f
done

if test 0 = `id -u`
then
    test -d /usr/src/publicfile-installer || mkdir -p /usr/src/publicfile-installer
    cd /usr/src/publicfile-installer
else
    test -d $HOME/.publicfile-installer || mkdir -p $HOME/.publicfile-installer
    cd $HOME/.publicfile-installer
fi

# set TARDIR/DIFFDIR
check_sources

if test -n "$TARDIR"
then
    echo "Found valid upstream .tar.gz in $TARDIR, no need to download"
else
    echo "Downloading $TARURL ..."
    wget --output-document=$TARFILE --quiet $TARURL
    if echo "$MD5SUM_TAR" | md5sum -c --status
    then
        TARDIR=`pwd`
    else
        echo "Downloaded upstream file $TARFILE seems corrupt. Giving up."
        exit 1
    fi
fi

if test -n "$DIFFDIR"
then
    echo "Found valid .debian.tar.xz in $DIFFDIR, no need to download"
else
    echo "Downloading http://mdcc.cx/pub/publicfile/$DSCFILE, $DIFFFILE ..."

    # cannot use dget since .orig.tar.gz is not mirrored
    wget --output-document=$DSCFILE --quiet http://mdcc.cx/pub/publicfile/$DSCFILE
    wget --output-document=$DIFFFILE --quiet http://mdcc.cx/pub/publicfile/$DIFFFILE
    if dscverify $DSCFILE
    then
        echo "Verified $DSCFILE"
    else
        echo "Verification of $DSCFILE failed; removing and giving up."
        rm $DSCFILE
        exit 1
    fi

    if echo "$MD5SUM_DIFF" | md5sum -c --status
    then
        DIFFDIR=`pwd`
    else
        echo "Downloaded file $DIFFFILE in `pwd` seems corrupt; removing and giving up."
        rm $DIFFFILE
        exit 1
    fi
fi

echo ",---------------------------- - - -  -"
echo "| $TARFILE verified and saved in $TARDIR."
echo "| $DIFFFILE verified and saved in $DIFFDIR."
echo " ---------------------------- - - -  -"

if test "$1" = "go"
then
    exec build-publicfile go
elif ask_user "Do you want to build the publicfile package now? [Yn] " 0
then
    exec build-publicfile
else
    echo "OK, you can run build-publicfile later."
fi
